import { provide, inject } from 'vue';
import { VisualDragEvent, VisualEventBus } from './Index.d';

// 用于拖拽的 provide inject
export const VisualDragProvider = (() => {
    const VISUAL_DRAG_PROVIDER = '@@VISUAL_DRAG_PROVIDER';
    return {
        provide: (data: VisualDragEvent) => {
            provide(VISUAL_DRAG_PROVIDER, data);
        },
        inject: () => {
            return inject(VISUAL_DRAG_PROVIDER) as VisualDragEvent;
        }
    }
})();

// 用于左侧菜单组件和容器组件通信的 provide inject
export const VisualEventBusProvider = (() => {
    const VISUAL_EVENTBUS_PROVIDER = '@@VISUAL_EVENTBUS_PROVIDER';
    return {
        provide: (data: VisualEventBus) => {
            provide(VISUAL_EVENTBUS_PROVIDER, data);
        },
        inject: () => {
            return inject(VISUAL_EVENTBUS_PROVIDER) as VisualEventBus;
        }
    }
})();
