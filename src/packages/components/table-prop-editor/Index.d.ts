import { VisualEditorProps } from '../visual-editor-props/Index.d';

// 表格
export interface TablePropEditorOption {
    data: any[];
    config: VisualEditorProps;
    onConfirm: (val: any[]) => void;
}