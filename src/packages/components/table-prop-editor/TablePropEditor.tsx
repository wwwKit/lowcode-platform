import { defineComponent, PropType } from 'vue';
// 组件
import $$TablePropEditorDialog from './TablePropEditorDialog';
import { ElTag, ElButton } from 'element-plus';
// 类型声明
import { VisualEditorProps } from '../visual-editor-props/Index.d';
// 工具函数
import { useModel } from '../../plugins/useModel';

// 右侧点击tag，添加属性
const TablePropEditor = defineComponent({
    props: {
        modelValue: { // 属性名, 如：type，size
            type: Array as PropType<any[]>
        },
        propConfig: { // 属性值
            type: Object as PropType<VisualEditorProps>,
            required: true
        }
    },
    emits: {
        'update:modelValue': (val?: any[]) => true
    },
    setup(props, ctx) {
        // 监听属性名，属性名变动
        const model = useModel(() => props.modelValue, val => ctx.emit('update:modelValue', val));
        
        const onClick = async () => {
            // 展示弹窗 data是添加的数据集合
            const data = await $$TablePropEditorDialog({
                config: props.propConfig,
                data: props.modelValue || []
            })
            model.value = data;
        }

        return () => (
            <div>
                {   // 如果没有值，则展示添加按钮
                    (!model.value || model.value.length == 0) && <ElButton {...{onClick} as any}>添加</ElButton>
                }
                {   // 如果有值，则展示tag
                    (model.value || []).map((item) => (
                        <ElTag {...{onClick} as any}>
                            {item[props.propConfig.table!.showKey]}
                        </ElTag>
                    ))
                }
            </div>
        )
    }
})

export default TablePropEditor;
