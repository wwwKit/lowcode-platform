import { defineComponent, PropType } from "vue";
import './Index.scss';
// 类型声明
import { VisualEditorModelValue, VisualEditorFocusData, VisualEditorState } from '../../Index.d';
import { VisualEditorBlockData } from '../visual-editor-block/Index.d';
// 组件
import $$dialog from '../import-export-dialog/Index';
import { ElMessageBox } from 'element-plus';

const VisualEditorHeader = defineComponent({
    props: {
        // 主数据
        dataModel: {
            type: Object as PropType<{ value: VisualEditorModelValue }>,
            required: true
        },
        // 公共状态
        state: {
            type: Object as PropType<VisualEditorState>,
            required: true
        },
        // 操作方法
        commander: {
            type: Object as any,
            required: true
        }
    },
    emits: [
        'updateDataModel',
        'clearFocus',
        'changePreviewState'
    ],
    setup(props, ctx) {
        // 公共方法
        const methods = {
            // 更新dataModel
            updateDataModel: (modelValue: VisualEditorModelValue) => {
                ctx.emit('updateDataModel', modelValue);
            },
            // 清除focus
            clearFocus: (blocks?: VisualEditorBlockData[]) => {
                ctx.emit('clearFocus', blocks);
            },
            // 修改preview状态
            changePreviewState: (isPreview: boolean) => {
                ctx.emit('changePreviewState', isPreview);
            }
        };
        const buttons: any = [
            [
                { label: '撤销', icon: 'el-icon-refresh-left', handler: props.commander.undo, tip: 'ctrl+z' },
                { label: '重做', icon: 'el-icon-refresh-right', handler: props.commander.redo, tip: 'ctrl+y, ctrl+shift+z' },
                {
                    label: () => props.state.preview ? '编辑' : '预览',
                    icon: () => props.state.preview ? 'el-icon-edit' : 'el-icon-view',
                    handler: () => {
                        methods.clearFocus();
                        methods.changePreviewState(!props.state.preview); // 切换预览或编辑状态
                    }
                }
            ],
            [
                {
                    label: '导入', icon: 'el-icon-upload2', handler: async () => {
                        const text = await $$dialog.textarea('', '请输入导入的JSON字符串');
                        try {
                            const data = JSON.parse(text || '');
                            methods.updateDataModel(data);
                        } catch (e) {
                            console.error(e);
                            ElMessageBox.alert('解析JSON错误');
                        }
                    }
                },
                {
                    label: '导出', icon: 'el-icon-download', handler: async () => $$dialog.textarea(JSON.stringify(props.dataModel.value), '导出的JSON数据', { editReadonly: true })
                }
            ],
            [
                { label: '置顶', icon: 'el-icon-top', handler: () => props.commander.placeTop(), tip: 'ctrl+up' },
                { label: '置底', icon: 'el-icon-bottom', handler: () => props.commander.placeBottom(), tip: 'ctrl+down' },
                { label: '删除', icon: 'el-icon-delete', handler: () => props.commander.delete(), tip: 'ctrl+d, backspace, delete' },
                { label: '清空', icon: 'el-icon-scissors', handler: () => props.commander.clear() }
            ]
        ]

        return () => <>
            <header class="visual-editor-header">
                <div class="visual-editor-header-wrap">
                    Vue3-Visual-Editor
                </div>
                {
                    buttons.map((buttonList: any, buttonListIndex: number) => (
                        <div class="visual-editor-header-wrap" key={`buttonList` + buttonListIndex}>
                            {
                                buttonList.map((button: any, index: number) => {
                                    const label = typeof button.label === 'function' ? button.label() : button.label;
                                    const icon = typeof button.icon === 'function' ? button.icon() : button.icon;
                                    return <div class="button" key={'button' + index} onClick={ button.handler }>
                                        <i class={ icon }></i>
                                        <p class="button-text">{ label }</p>
                                    </div>
                                })
                            }
                        </div>
                    ))
                }
            </header>
        </>
    }
})

export default VisualEditorHeader;
