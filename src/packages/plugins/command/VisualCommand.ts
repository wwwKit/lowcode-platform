// 类型声明
import { VisualEditorBlockData } from '../../components/visual-editor-block/Index.d';
import { VisualEditorModelValue } from '../../Index.d';
// 引入注册函数
import { useCommander } from './Command.plugin';
import deepcopy from 'deepcopy';

// 注册其他点击事件
export function useVisualCommand({ dataModel, focusData, updateBlocks, dragStart, dragEnd }: {
    dataModel: { value: VisualEditorModelValue },
    focusData: { value: { focus: VisualEditorBlockData[], unFocus: VisualEditorBlockData[] } },
    updateBlocks: (blocks?: VisualEditorBlockData[]) => void,
    dragStart: { on: (cb: () => void) => void, off: (cb: () => void) => void },
    dragEnd: { on: (cb: () => void) => void, off: (cb: () => void) => void }
}) {
    // 注册函数
    const commander = useCommander();
    commander.init();

    // 注册删除
    commander.register({
        name: 'delete',
        keyboard: [
            'backspace',
            'delete',
            'ctrl+d'
        ],
        followQueue: true,
        execute: () => {
            console.log('执行删除命令');
            let data = {
                before: dataModel.value.blocks,
                after: focusData.value.unFocus
            }
            return {
                undo: () => {
                    console.log('撤回删除命令');
                    updateBlocks(data.before);
                },
                redo: () => {
                    console.log('redo删除命令');
                    updateBlocks(data.after);
                }
            }
        }
    })

    // 注册拖拽
    commander.register({
        name: 'drag',
        followQueue: true,
        init() {
            this.data = {
                before: null as null | VisualEditorBlockData[]
            }
            const handler = {
                dragstart: () => {
                    this.data.before = deepcopy(dataModel.value.blocks);
                },
                dragend: () => {
                    commander.state.commands.drag(); // 其实就是redo，并且将拖拽redo audo保存一份到数组queue中，用于撤销操作
                }
            }
            // 添加订阅
            dragStart.on(handler.dragstart);
            dragEnd.on(handler.dragend);
            // 销毁订阅
            return () => {
                console.log('拖拽-销毁订阅');
                dragStart.off(handler.dragstart);
                dragEnd.off(handler.dragend);
            }
        },
        execute() {
            let before = this.data.before;
            let after = deepcopy(dataModel.value.blocks);
            return {
                undo: () => {
                    console.log('undo撤回拖拽命令', before);
                    updateBlocks(before);
                },
                redo: () => {
                    console.log('redo拖拽命令', after);
                    updateBlocks(after);
                }
            }
        }
    })

    // 注册清空
    commander.register({
        name: 'clear',
        followQueue: true,
        execute() {
            let data = {
                before: deepcopy(dataModel.value.blocks),
                after: []
            }
            return {
                undo: () => {
                    console.log('undo撤回清空命令');
                    updateBlocks(data.before);
                },
                redo: () => {
                    console.log('redo清空命令');
                    updateBlocks(data.after);
                }
            }
        }
    })

    // 注册置顶
    commander.register({
        name: 'placeTop',
        keyboard: 'ctrl+up',
        followQueue: true,
        execute() {
            let data = {
                before: deepcopy(dataModel.value.blocks),
                after: (() => {
                    const { focus, unFocus } = focusData.value;
                    const maxZIndex = unFocus.reduce((prev, block) => Math.max(prev, block.zIndex), 0) + 1;
                    // 给选中元素增加权重
                    focus.forEach(block => block.zIndex = maxZIndex);
                    return deepcopy(dataModel.value.blocks);
                })()
            }
            return {
                undo: () => {
                    console.log('undo撤回置顶命令', data.before);
                    updateBlocks(deepcopy(data.before));
                },
                redo: () => {
                    console.log('redo置顶命令', data.after);
                    updateBlocks(deepcopy(data.after));
                }
            }
        }
    })

    // 注册置底
    commander.register({
        name: 'placeBottom',
        keyboard: 'ctrl+down',
        followQueue: true,
        execute() {
            let data = {
                before: deepcopy(dataModel.value.blocks),
                after: deepcopy((() => {
                    const { focus, unFocus } = focusData.value;
                    let minZIndex = unFocus.reduce((prev, block) => Math.min(prev, block.zIndex), Infinity) - 1;
                    if (minZIndex < 0) { // 小于0,就让他等于0（小于0不太好）
                        const dur = Math.abs(minZIndex);
                        unFocus.forEach(block => block.zIndex += dur);
                        minZIndex = 0;
                    }
                    // 给选中元素增加权重
                    focus.forEach(block => block.zIndex = minZIndex);
                    return deepcopy(dataModel.value.blocks);
                })())
            }
            return {
                undo: () => {
                    console.log('undo撤回置底命令', data.before);
                    updateBlocks(deepcopy(data.before));
                },
                redo: () => {
                    console.log('redo置底命令', data.after);
                    updateBlocks(deepcopy(data.after));
                }
            }
        }
    })

    // 注册，全选
    commander.register({
        name: 'selectAll',
        keyboard: 'ctrl+a',
        followQueue: false,
        execute: (val: VisualEditorModelValue) => {
            return {
                undo: () => {
                    console.log('undo撤回全选命令');
                },
                redo: () => {
                    console.log('redo全选命令');
                    (dataModel.value.blocks || []).forEach(block => block.focus = true);
                }
            }
        }
    })

    return {
        undo: () => commander.state.commands.undo(), // 内部，作用是执行queue中的undo
        redo: () => commander.state.commands.redo(), // 内部，作用是执行queue中的redo
        delete: () => commander.state.commands.delete(), // 外部注册 删除
        drag: () => commander.state.commands.drag(), // 外部注册 拖拽
        clear: () => commander.state.commands.clear(), // 外部注册 清空
        placeTop: () => commander.state.commands.placeTop(), // 外部注册 置顶
        placeBottom: () => commander.state.commands.placeBottom() // 外部注册 置底
    }
}
