module.exports = {
    publicPath: './',
    css: {
        extract: true,
        sourceMap: false,
        loaderOptions: {
            scss: {
                prependData: `
                    @import "~@/assets/styles/mixin.scss";
                    @import "~@/assets/styles/_variable.scss";
                `
            }
        }
    }
}